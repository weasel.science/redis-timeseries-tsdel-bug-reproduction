FROM ubuntu:18.04 AS builder
RUN apt-get update && apt-get install -y git build-essential
RUN git clone --recursive https://github.com/RedisTimeSeries/RedisTimeSeries.git
RUN cd RedisTimeSeries && git checkout "origin/1.6" && ./deps/readies/bin/getpy3 && ./system-setup.py && make build

FROM redis:6.2.6 AS redis-source

FROM node:16-bullseye
COPY --from=builder /RedisTimeSeries/bin/redistimeseries.so /redistimeseries.so
COPY --from=redis-source /usr/local/bin/redis-server /redis-server
ADD . /app
WORKDIR /app
RUN npm install
CMD ["bash", "./startup.sh"]