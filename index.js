import ioredis from 'ioredis';

const redis = new ioredis();

const timeseriesName = 'subject';

const from = new Date('2022-01-01').valueOf();
const to = new Date('2022-03-01').valueOf();

const delete_from = new Date('2022-01-10').valueOf();
const delete_to = new Date('2022-01-20').valueOf();

const main = async () => {
  console.log(`Step 1: Create Time series ${timeseriesName}`);
  await redis.del(timeseriesName);
  await redis.call('TS.CREATE', timeseriesName);

  console.log(`Step 2: Populate time series`)
  await redis.call('TS.MADD', ...getMaddCommandArgs({ from, to }));

  let numberOfSamples = (await redis.call('TS.RANGE', timeseriesName, '-', '+')).length;
  console.log(`Step 3: Current Number of Samples using the Range query: ${numberOfSamples}`);

  const numberOfRemovedSamples = await redis.call('TS.DEL', timeseriesName, delete_from, delete_to);
  console.log(`Step 4: Removed ${numberOfRemovedSamples} samples`);

  numberOfSamples = (await redis.call('TS.RANGE', timeseriesName, '-', '+')).length;
  console.log(`Step 5: Samples after removal: ${numberOfSamples} - this should be roughly identical to step 3 minus step 4`);

  console.log(`Step 6: Populate Time series in the period that was deleted before`);
  await redis.call('TS.MADD', ...getMaddCommandArgs({ from: delete_from, to: delete_to }));

  numberOfSamples = (await redis.call('TS.RANGE', timeseriesName, '-', '+')).length;
  console.log(`Step 7: Current Number of Samples using the Range query: ${numberOfSamples}. This should be roughly identical to the number in step 3.`);

  process.exit(0);
};

const getMaddCommandArgs = ({
  from,
  to
}) => {
  const step = 30 * 60 * 1000; // 30 minutes in milliseconds
  const commandArguments = [];

  let counter = 0;
  for (let i = from; i < to; i += step) {
    commandArguments.push(timeseriesName, i, Math.random());
    counter += 1;
  }
  console.log(`Generated ${counter} samples.`);

  return commandArguments;
};

main();
