Code in this repository reproduces an issue with Redis TimeSeries TS.DEL command.

Main reproduction code is in `index.js`

To summarize:
* Create a new timeseries called `subject`
* Populate it with TS.MADD for two months time, from January 1st 2022 till March 1st 2022, in intervals of 30 minutes, with random numbers for data.
* Use TS.DEL to delete samples from January 10th 2021 till January 20th 2022.
* Repopulate the above deleted samples using TS.MADD, again in intervals of 30 minutes, with random data.
* Run `TS.MRANGE subject - +` and observe that data until around January 13th 2022 is missing from the response. 

How to run this:
* `docker build . -t redis-ts-bug-repro`
* `docker run -it --rm redis-ts-bug-repro`

To run without docker:
* Run Redis with TimeSeries module (checked out to origin/1.6) and `DUPLICATE_POLICY LAST`
* Run `npm install` (with node 16 or newer)
* Run `npm run start` to observe the reproduction.
